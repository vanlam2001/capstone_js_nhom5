let error = [
  "Vui lòng nhập tên Sản Phẩm",
  "Vui lòng nhập giá",
  "Vui lòng nhập Màn hình",
  "Vui lòng nhập màu Camera",
  "Vui lòng nhập front Camera",
  "Vui lòng dán hình ảnh",
  "Vui lòng nhập mô tả",
];

function kiemTraRong(el, message, idNotify) {
    if (el == "") {
      return (
        (document.getElementById(idNotify).innerHTML = error[message]),
        (document.getElementById(idNotify).style.display = `block`),
        false
      );
    } else {
      return (
        (document.getElementById(idNotify).innerHTML = ""),
        (document.getElementById(idNotify).style.display = "none"),
        true
      );
    }
}


function kiemTraChon() {
  var flag = true ; 
    var loai = document.getElementById('type').value ; 
    if (loai == "Chọn loại điện thoại" || loai == "") {
        document.getElementById('tb__loai').innerHTML = "Vui lòng chọn loại điện thoại" ; 
        flag = false ; 
    }else {
        document.getElementById('tb__loai').innerHTML = "" ; 
        flag = true ;
    }
    return flag ; 
}

