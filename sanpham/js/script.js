const BASE_URL = "https://63d93875baa0f79e09b795eb.mockapi.io";
var DS = [];
var filteredProductList = [];
var cart = [];

function fetchQLSPList() {
  batLoading();
  axios({
    url: `${BASE_URL}/products`,
    method: "GET",
  })
    .then(function (res) {
      renderDanhSachSP(res.data);
      DS.push(...res.data);
    })

    .catch(function (err) {
      console.log("🚀 ~ file: script.js:15 ~ fetchQLSPList ~ err", err);
      tatLoading();
    });
}

fetchQLSPList();

function timKiem() {
  let search = document.getElementById("textsearch").value;
  let spSearch = DS.filter(function (value) {
    return value.name.toUpperCase().includes(search.toUpperCase());
  });
  renderDanhSachSP(spSearch);
}

document
  .getElementById("productTypeSelect")
  .addEventListener("change", function () {
    var selectedType = this.value;

    filteredProductList = DS.filter(function (product) {
      return product.type === selectedType;
    });

    if (filteredProductList.length === 0) {
      return renderDanhSachSP(DS);
    }

    renderDanhSachSP(filteredProductList);
  });

var cart = [];
function themSP(id) {
  var viTri = timKiemViTri(id, DS);
  var spduocchon = DS[viTri];
  var sp = new CartItem(spduocchon.id, spduocchon.name, spduocchon.price, 1);
  if (cart.length == 0) {
    cart.push(sp);
  } else {
    var viTri = cart.findIndex(function (item) {
      return item.id == spduocchon.id;
    });
    if (viTri != -1) {
      cart[viTri].quantity++;
    } else {
      cart.push(sp);
    }
  }

  document.getElementById("thongbaosoluong").innerHTML = `${cart.length}`;
  localStorage.setItem("GioHang", JSON.stringify(cart));
  Toastify({
    text: "Thêm vào giỏ hàng thành công",

    duration: 1000,
  }).showToast();
}
