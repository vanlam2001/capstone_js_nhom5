function renderGioHang(qlspArr) {
    console.log("🚀 ~ file: controller.js:2 ~ renderDanhSachSP ~ qlspArr", qlspArr)
    var contentHTML = "";
  
    qlspArr.forEach(function (DS) {
      var content = `
      <tr>
      <td>${DS.name}</td>
      <td>${DS.price * DS.quantity}</td>
      <td>
          <div class="buttons_added">
              <button style = "display: inline-block;
              width: 30px;
              height: 30px;
              border: 1px solid white;"
              onclick = "tangSL(${DS.id})">+</button>
              <span style = "display : inline-block ; margin : 0 10px">${DS.quantity}</span>
              <button style = "display: inline-block;
              width: 30px;
              height: 30px;
              border: 1px solid white;"
              onclick = "giamSL(${DS.id})">-</button>
          </div>
      </td>
      <td><button onclick = "xoaSP(${DS.id})" class = "btn btn-danger">Xóa</button></td>
  </tr>
      
          `;
  
      contentHTML += content;
    });
  
    document.getElementById('danhsachgiohang').innerHTML = contentHTML;
  }

function tongTien(arr) { 
    var tong = 0 ; 
    arr.forEach(element => {
        var soTien = element.price * element.quantity ;
        tong += soTien ; 
    });
    document.getElementById('tongsotien').innerHTML = tong ; 
}


function timKiemViTri(id , arr){
    var viTri = arr.findIndex(function(item){
        return item.id == id ; 
    })
    return viTri ; 
}