var cartProduct = JSON.parse(localStorage.getItem("GioHang"));

renderGioHang(cartProduct);
tongTien(cartProduct);

function tangSL(id) {
  var cartProduct = JSON.parse(localStorage.getItem("GioHang"));
  var viTri = timKiemViTri(id, cartProduct);
  console.log("🚀 ~ file: cart_main.js:14 ~ viTri ~ viTri", viTri);
  var element = cartProduct[viTri];
  element.quantity++;

  localStorage.setItem("GioHang", JSON.stringify(cartProduct));
  cartProduct = JSON.parse(localStorage.getItem("GioHang"));

  renderGioHang(cartProduct);
  tongTien(cartProduct);
}

function giamSL(id) {
  var cartProduct = JSON.parse(localStorage.getItem("GioHang"));
  var viTri = timKiemViTri(id, cartProduct);
  console.log("🚀 ~ file: cart_main.js:31 ~ viTri ~ viTri", viTri);
  var element = cartProduct[viTri];
  element.quantity--;

  localStorage.setItem("GioHang", JSON.stringify(cartProduct));
  cartProduct = JSON.parse(localStorage.getItem("GioHang"));

  renderGioHang(cartProduct);
  tongTien(cartProduct);
}

function xoaSP(id) {
  var cartProduct = JSON.parse(localStorage.getItem("GioHang"));
  var viTri = timKiemViTri(id, cartProduct);
  console.log("🚀 ~ file: cart_main.js:49 ~ viTri ~ viTri", viTri);
  cartProduct.splice(viTri, 1);

  localStorage.setItem("GioHang", JSON.stringify(cartProduct));
  cartProduct = JSON.parse(localStorage.getItem("GioHang"));

  renderGioHang(cartProduct);
  tongTien(cartProduct);
}

function thanhToan() {
  localStorage.removeItem("GioHang");
  Toastify({
    text: "Thanh toán thành công",

    duration: 1000,
  }).showToast();
  var arrCart = localStorage.getItem("GioHang")
    ? JSON.parse(localStorage.getItem("GioHang"))
    : [];
  renderGioHang(arrCart);
  document.getElementById("tongsotien").innerHTML = 0;
}
